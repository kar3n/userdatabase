package filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebFilter(filterName = "userProfilePageFilter", urlPatterns = "/userProfilePage")
public class userProfilePageFilter implements Filter
{
	public void destroy()
	{
	
	}
	
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException
	{
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) resp;
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		HttpSession session = request.getSession(false);
		
		if (session.getAttribute("login") != null)
		{
			chain.doFilter(req, resp);
		}
		else
		{
			out.println("Please login first...");
			request.getRequestDispatcher("signInPage.jsp").include(request, response);
		}
		
		
	}
	
	public void init(FilterConfig config) throws ServletException
	{
		
	}
	
}
