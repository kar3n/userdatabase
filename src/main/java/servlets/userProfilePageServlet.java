package servlets;

import bean.User;
import service.UserRepository;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "userProfilePageServlet", urlPatterns = "/userProfilePage")
public class userProfilePageServlet extends HttpServlet
{
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType("text/html");
		HttpSession session = request.getSession();
		User user = UserRepository.getInstance().getUserByLogin(String.valueOf(session.getAttribute("login")));
		PrintWriter out = response.getWriter();
		out.println("Logged in as [" + user.getName() + "] ");
		request.setAttribute("name", user.getName());
		request.getRequestDispatcher("userProfilePage.jsp").include(request, response);
	
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		
	}
}
