package servlets;

import bean.User;
import service.UserRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet(name = "UserDeletingServlet", urlPatterns = "/removeAccount")
public class UserDeletingServlet extends HttpServlet
{
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String login = String.valueOf(request.getSession().getAttribute("login"));
		User user = UserRepository.getInstance().getUserByLogin(login);
		UserRepository.getInstance().removeUser(user);
		out.println("Account: " + user.getLogin() + " successfully removed from database");
		request.getRequestDispatcher("signInPage.jsp").include(request, response);
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
	
	}
}
