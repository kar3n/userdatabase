package servlets;

import bean.User;
import service.UserRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "SignInServlet", urlPatterns = "/signInPage")
public class SignInServlet extends HttpServlet
{
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		//service(request, response);
		String buttonPressed = request.getParameter("button");
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		if (buttonPressed.equals("signInButton"))
		{
			String login = request.getParameter("loginInput");
			String password = request.getParameter("passwordInput");
			User tempUser = new User(login, password);
			
			if (UserRepository.getInstance().checkUserExistence(tempUser))
			{
				User realUser = UserRepository.getInstance().getUserByLogin(tempUser.getLogin());
				HttpSession session = request.getSession();
				session.setAttribute("login", realUser.getLogin());
				request.getRequestDispatcher("signInOK.jsp").forward(request, response);
			}
			else
			{
				out.println("Wrong Login or Password...");
				request.getRequestDispatcher("signInPage.jsp").include(request, response);
			}
		}
		else
		{
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		
	}
}
