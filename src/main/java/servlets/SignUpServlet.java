package servlets;

import bean.User;
import service.UserRepository;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "SignUpServlet", urlPatterns = "/signUpPage")
public class SignUpServlet extends javax.servlet.http.HttpServlet
{
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, IOException
	{
		String buttonPressed = request.getParameter("button");
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String name = request.getParameter("signUpUserNameInput");
		String login = request.getParameter("signUpLoginInput");
		String password = request.getParameter("signUpPasswordInput");
		User user = new User(name, login, password);
		
		
		if (buttonPressed.equals("signUpButton"))
		{
			if (login.equals("") || password.equals("") || name.equals(""))
			{
				out.println("empty fields detected");
				request.getRequestDispatcher("signUpPage.jsp").include(request, response);
			}
			else if (UserRepository.getInstance().checkUserExistence(user))
			{
				out.println("Login already taken");
				request.getRequestDispatcher("signUpPage.jsp").include(request, response);
			}
			else
			{
				UserRepository.getInstance().addUser(user);
				out.println("Registration successful you may login now");
				request.getRequestDispatcher("signInPage.jsp").include(request, response);
			}
		}
		else
		{
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, IOException
	{
		
	}
}
