package service;

import bean.User;

public interface UserService
{
	User addUser(User user);
	
	User removeUser(User user);
	
	User getUserByLogin(String login);
	
	boolean checkUserExistence(User inputObject);
}
