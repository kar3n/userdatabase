package service;

import bean.User;

import java.util.HashMap;
import java.util.Map;

public class UserRepository implements UserService
{
	private static UserRepository ourInstance = new UserRepository();
	private Map<String, User> userRepo;
	
	
	public static UserRepository getInstance()
	{
		return ourInstance;
	}
	
	private UserRepository()
	{
		userRepo = new HashMap<>();
	}
	
	@Override
	public User addUser(User user)
	{
		userRepo.put(user.getLogin(), user);
		return user;
	}
	
	@Override
	public User removeUser(User user)
	{
		userRepo.remove(user.getLogin());
		return user;
	}
	
	@Override
	public User getUserByLogin(String login)
	{
		return userRepo.get(login);
	}
	
	@Override
	public boolean checkUserExistence(User inputObject)
	{
		if (userRepo.get(inputObject.getLogin()) == null)
		{
			return false;
		}
		else {
			return userRepo.get(inputObject.getLogin()).equals(inputObject);
		}
	}
}
