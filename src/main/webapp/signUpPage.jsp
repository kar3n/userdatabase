<%--
  Created by IntelliJ IDEA.
  User: kar3n
  Date: 14-Jul-19
  Time: 8:32 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Sign Up</title>
</head>
<body>
<h3>Please fill the information below to sign up</h3>
<hr>

<form method="post" action="signUpPage">

    User Name <br>
    <input type="text" name="signUpUserNameInput"> <br>
    Login <br>
    <input type="text" name="signUpLoginInput"> <br>
    Password <br>
    <input type="password" name="signUpPasswordInput"> <br>
    <button type="submit" name="button" value="signUpButton">Sign Up</button><br><br>
    <button type="submit" name="button" value="toWelcomePage">Back</button><br><br>

</form>

</body>
</html>
